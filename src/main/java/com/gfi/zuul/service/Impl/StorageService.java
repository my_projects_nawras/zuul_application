package com.gfi.zuul.service.Impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import com.gfi.zuul.dao.UserRepository;
import com.gfi.zuul.model.User;
import com.gfi.zuul.service.IStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StorageService implements IStorageService {
    @Autowired
    UserRepository userRepository;
    Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final Path rootLocation = Paths.get("upload-dir");

    public void store(MultipartFile file, Long id_user) {
        try {
            byte[] byteArr = file.getBytes();
            Optional<User> user = this.userRepository.findById(id_user);
            if (user.isPresent()) {
                Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
                user.get().setImageByte(byteArr);
                userRepository.save(user.get());
                Optional<User> user1 = this.userRepository.findById(id_user);
                System.out.println(new String(user1.get().getImageByte()));
            }
        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }
}
