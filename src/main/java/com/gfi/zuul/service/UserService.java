package com.gfi.zuul.service;
import com.gfi.zuul.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllusers();

    boolean saveUser(User utilisateur);

    boolean deleteUser(User utilisateur);

    Optional<List<User>> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findById(Long id);


    boolean updateUsername(String email, String username);

    boolean updatePassword(String username, String oldPass, String newPAss);

    boolean updateImageName(String email, String imgName);

    User findByEmailAndPassword(String email, String password);

    int getNubmerBookDoneOfUser(String email);

    String signin(String email, String password);

    String signup(User user);

    String getUserImage(Long id);
}
