package com.gfi.zuul.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.node.ObjectNode;

import com.gfi.zuul.dao.RoleRepository;
import com.gfi.zuul.dao.UserRepository;
import com.gfi.zuul.dto.ImageResponseDTO;
import com.gfi.zuul.message.request.LoginForm;
import com.gfi.zuul.message.response.JwtResponse;
import com.gfi.zuul.model.User;
import com.gfi.zuul.security.jwt.JwtProvider;
import com.gfi.zuul.security.services.UserPrinciple;
import com.gfi.zuul.service.Impl.UserServiceImpl;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserServiceImpl utilisateurService;
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    JwtResponse jwtresponse;

    User user;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {


        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(

                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        Long user = ((UserPrinciple) authentication.getPrincipal()).getId();


        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetail = (UserDetails) authentication.getPrincipal();


        return ResponseEntity.ok(new JwtResponse(user, jwt, userDetail.getUsername(), userDetail.getAuthorities()));
    }


    @GetMapping
    public List<User> getAllUsers() {
        return this.utilisateurService.getAllusers();
    }



    @PostMapping("/signup")
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 403, message = "Access denied"), //
            @ApiResponse(code = 422, message = "Username is already in use"), //
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    public ResponseEntity<Map<String, String>> signup(@ApiParam("Signup User") @RequestBody User user) throws IOException {
        //String example = "C:\\Users\\User\\IdeaProjects\\gedapplication\\upload-dir";
        Path path = Paths.get("C:\\Users\\salim\\IdeaProjects\\zuul-master\\upload-dir\\default.png");
        FileInputStream fileInputStream = new FileInputStream(path.toFile());
        byte[] fileContent = new byte[(int) path.toFile().length()];
        fileInputStream.read(fileContent);


        user.setImageByte(fileContent);

        return new ResponseEntity<Map<String, String>>(Collections.singletonMap("token", utilisateurService.signup(user)), HttpStatus.OK);
    }

    @GetMapping(path = "/getUserImageName/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getUserImageName(@PathVariable(value = "id", required = true) Long id
    ) {
        ImageResponseDTO imageResponseDTO = new ImageResponseDTO();
        Optional<User> user = this.utilisateurService.findById(id);
        if (user.isPresent()) {
            if (user.get().getImageByte() != null) {
                System.out.println(new String(user.get().getImageByte()));
                imageResponseDTO.setImg(user.get().getImageByte());
                imageResponseDTO.setSucces(1);
            }
        }

        return imageResponseDTO.getImg();
    }

    @GetMapping(path = "/checkMail")
    public ResponseEntity<Boolean> checkEmail(@RequestParam(value = "email", required = true) String email
    ) {
        Optional<User> opt = this.utilisateurService.findByEmail(email);
        if (opt.isPresent())
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        else
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);

    }

    @PostMapping(path = "/updateUsername")
    public ResponseEntity<Boolean> updateUsername(@RequestBody ObjectNode json
    ) {
        String email;
        String username;
        try {
            email = new ObjectMapper().treeToValue(json.get("email"), String.class);
            username = new ObjectMapper().treeToValue(json.get("username"), String.class);
            boolean test = this.utilisateurService.updateUsername(email, username);
            if (test)
                return new ResponseEntity<Boolean>(test, HttpStatus.OK);

        } catch (JsonProcessingException e) {
            System.out.println("Parsing Exception!!");
            e.printStackTrace();
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_ACCEPTABLE);

        }
        return new ResponseEntity<Boolean>(false, HttpStatus.NOT_ACCEPTABLE);


    }

    @PostMapping(path = "/updateImageName")
    public ResponseEntity<Boolean> updateImageName(@RequestBody ObjectNode json
    ) {
        String email;
        String imageName;
        try {
            email = new ObjectMapper().treeToValue(json.get("email"), String.class);
            imageName = new ObjectMapper().treeToValue(json.get("imageName"), String.class);
            boolean test = this.utilisateurService.updateImageName(email, imageName);
            if (test)
                return new ResponseEntity<Boolean>(test, HttpStatus.OK);

        } catch (JsonProcessingException e) {
            System.out.println("Parsing Exception!!");
            e.printStackTrace();
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_ACCEPTABLE);

        }
        return new ResponseEntity<Boolean>(false, HttpStatus.NOT_ACCEPTABLE);


    }


    @PostMapping(path = "/updatePassword")
    public ResponseEntity<Boolean> updatePassword(@RequestBody ObjectNode json
    ) {
        String username;
        String oldPass;
        String newPass;

        try {
            username = new ObjectMapper().treeToValue(json.get("username"), String.class);
            oldPass = new ObjectMapper().treeToValue(json.get("oldPass"), String.class);
            newPass = new ObjectMapper().treeToValue(json.get("newPass"), String.class);

            boolean test = this.utilisateurService.updatePassword(username, oldPass, newPass);
            if (test)
                return new ResponseEntity<Boolean>(test, HttpStatus.OK);

        } catch (JsonProcessingException e) {
            System.out.println("Parsing Exception!!");
            e.printStackTrace();
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_ACCEPTABLE);

        }
        return new ResponseEntity<Boolean>(false, HttpStatus.NOT_ACCEPTABLE);


    }

    @GetMapping(path = "/getUser")
    public ResponseEntity<User> getUser(@RequestParam(value = "email", required = true) String email) {
        User user = this.utilisateurService.findByEmail(email).get();

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }


}
