package com.gfi.zuul.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponseDTO {
    private int succes;
    private String message;

    public BaseResponseDTO(int succes, String message) {
        this.succes = succes;
        this.message = message;
    }

    public BaseResponseDTO() {
    }

    public int getSucces() {
        return succes;
    }

    public void setSucces(int succes) {
        this.succes = succes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
